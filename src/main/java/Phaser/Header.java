package Phaser;
import Phaser.ClassObject.*;
import info.debatty.java.stringsimilarity.JaroWinkler;
import java.text.SimpleDateFormat;
import java.util.*;

class Header{


    // для заголовков
    private static String[][] headers_option = {};

    private static String[][] headers_option_months = {};

    /**
     * Используется в цикле перебора строки,
     *  указывает были ли принят заголовок.
     */
    private static Boolean installed = false;

    // Проверка на наличия заголовков
    static boolean hasHeader()
    {
        return Phaser.header.size() > 0;
    }

    // Get the type by id
    static String getTypeCell( Integer itr )
    {
        // if there is no header
        if( Phaser.header.size() < 1 ) return "";

        // try to get a header type by key.
        if( Phaser.header.get( itr ) != null )
            return Phaser.header.get( itr ).type;
        else return "";
    }

    /**
     * Set the header before reading the grid
     * @param fileKey
     */
    void setHeaderManually( String fileKey )
    {
        // get headers
        headers_option = RequestsToServer.getHeaders( fileKey );

        int i = 0;
        for( String[] head : headers_option )
        {
            // create the header
            _setHeader(
                    i,                           // iterator
                    head[0].toLowerCase(),       // name
                    head[1],                     // type
                    head[2],                     // key
                    Integer.parseInt( head[4] ), // priority
                    Integer.parseInt( head[5] )  // priority_any
            );
            i++;
        }
    }

    // Проверка существования заголовка по названии
    private boolean headerExistByKey( String key )
    {
        // если список заголовков пуст
        if( Phaser.header.size() == 0 ) return false;

        // перебирваю заголовки
        for( Map.Entry<Integer, ObjHeader> entry : Phaser.header.entrySet() )
            // сравниваю
            if( key.equals( entry.getValue().key ) )
                return true;

        // если ничег не найдено
        return false;

    }

    /**
     * Get header by name
     *
     * @param key String
     */
    private ObjHeader getHeaderByKey( String key )
    {
        // если список заголовков пуст
        if( Phaser.header.size() == 0 ) new ObjHeader( 0, "", "", "", 0, 0 );

        // перебирваю заголовки
        for( Map.Entry<Integer, ObjHeader> entry : Phaser.header.entrySet() )
            // сравниваю
            if( key.equals( entry.getValue().key ) )
                return entry.getValue();

        // if nothing is found
        return new ObjHeader( 0, "", "", "", 0, 0 );

    }

    // Создание заголовков
    void createHeader()
    {
        /*
          Проверяю наличие заголовка,
           и пустоту строку.
         */
        if( ! hasHeader() && Phaser.rowNotEmpty )
        {
            // Класс для сравнивания строк
            JaroWinkler jw = new JaroWinkler();

            // если массив с заголовками пуст
            if( headers_option.length < 1 )
            {
                // получаю варианты заголовков
                headers_option = RequestsToServer.getHeaders( false );
                headers_option_months = RequestsToServer.getHeaders( true );
            }

            String s1, s2;

            // перебираю строку
            for( Map.Entry<Integer, String> cell : Phaser.cRow.entrySet() )
            {
                installed = false;

                // итератор ячейки
                Integer i = cell.getKey();

                // пропускаю пустые елементы
                if( cell.getValue().length() == 0 ) continue;

                // Если удалось определить месяц, переходим к следующей ячейке
                if( detectedMonthType( cell.getValue(), i ) ) continue;

                // строка из таблицы, подготовка строки
                s2 = Helper.preparationStr( cell.getValue(), new String[]{"."} );

                // если строка пустая, пропускаем
                if( s2.length() == 0 ) continue;

                /*
                  перебираю примеры заголовков
                  сравниваю строки,
                  если совпало, добавляю в список ключ заголовков
                 */
                for( String[] hOpt : headers_option )
                {
                    // получаю тип сравнения
                    int compar = Integer.parseInt( hOpt[3] );

                    // строка из вариантов заголовка, подготовка строки
                    s1 = Helper.preparationStr( hOpt[0], new String[]{"."} );

                    // определяю тип сравнения,
                    // и провожу стравнение
                    switch( compar )
                    {
                        case Option.COMPARISON_TYPE__EQUALS:
                            if( s2.equals( s1 ) ) continue;
                            break;
                        case Option.COMPARISON_TYPE__INDEX_OF:
                            if( s2.contains( s1 ) ) continue;
                            break;
                        case Option.COMPARISON_TYPE__SIMILAR:
                            if( jw.similarity( s1, s2 ) > Option.RATE_SIMILAR ) continue;
                            break;
                        case Option.COMPARISON_TYPE_EQUALS:
                            if( s2.equals( s1 ) )
                                this.checkPriority( s2, i, hOpt[1], hOpt[2], Integer.valueOf( hOpt[4] ), Integer.valueOf( hOpt[5] ) );
                            break;
                        case Option.COMPARISON_TYPE_INDEX_OF:
                            if( s2.contains( s1 ) )
                                this.checkPriority( s2, i, hOpt[1], hOpt[2], Integer.valueOf( hOpt[4] ), Integer.valueOf( hOpt[5] ) );
                            break;
                        case Option.COMPARISON_TYPE_SIMILAR:
                            if( jw.similarity( s1, s2 ) > Option.RATE_SIMILAR )
                                this.checkPriority( s2, i, hOpt[1], hOpt[2], Integer.valueOf( hOpt[4] ), Integer.valueOf( hOpt[5] ) );
                            break;
                    }
                }

                // если заголовок не был установлен
                if( ! installed )
                {
                    // build a key
                    String _key = "addition__" + s2;

                    // set a header
                    this._setHeader( i, s2, "string", _key, 0, 0 );
                }
            }

            /*
              Если список заголовком меньше,
               чем заданный минимум
             */
            if( Phaser.header.size() < Option.HEADER_MINIMUM )
            {
                // очищаю список
                Phaser.header.clear();

                // Проверяю строку на наличие города
                Phaser.isCityRow();
            }

        }
    }

    /**
     * Определение месяца
     *
     * @param s            - строка ячейки
     * @param cellIterator - итератор ячейки
     */
    private boolean detectedMonthType( String s, Integer cellIterator )
    {
        // флаг указатель
        boolean flag = true;

        s = replaceMFT( s );

        // строка для сравнения
        String s2 = s;

        // если нету букв в строке
        if( ! Helper.hasLetter( s ) )
        {
            // получаю месяц и год по шаблону даты
            int[] _month_year = _detectedDateByPattern( s );

            if( _month_year[0] != 0 && _month_year[1] != 0 )
            {
                // записываю месяц, дабвляя нули к числам менее 9. И через точку год
                s = String.valueOf( _month_year[1] < 10 ? "0" + _month_year[1] : _month_year[1] ) + "." + _month_year[0];
            }
        }


        int cYear = 0;

        // если в строке нашлась цифра
        if( Helper.hasInt( s ) )
        {
            // получаю текущий год, в разных форматах
            int yearF = Integer.valueOf( Helper.getDate( "yyyy" ) );
            int yearS = Integer.valueOf( Helper.getDate( "yy" ) );

            // массив с годами в разных форматах и +- 1 год
            int[] years = {yearF - 1, yearF, yearF + 1, yearS - 1, yearS, yearS + 1};

            // перебираю года
            for( int year : years )
            {
                cYear = year;

                // search year in the string
                if( s.contains( String.valueOf( year ) ) )
                {
                    // если год в full формате
                    if( year > 1000 )
                    {
                        // если год не является прошлым
                        if( year >= yearF )
                        {
                            // вычетаю год из строки
                            s2 = s.replace( String.valueOf( year ), "" );
                            flag = true;
                            break;
                        } else
                        {
                            flag = false;
                            break;
                        }
                    } else
                    {
                        // есил год не является прошлым
                        if( year >= yearS )
                        {
                            // вычетаю год из строки
                            s2 = s.replace( String.valueOf( year ), "" );
                            flag = true;
                            break;
                        } else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
            }
        }

        // если указатель положительный,
        // значит удалось определить год или там нету цифр
        if( flag )
        {
            // очищаю строку от лишнего, и в нижний регистр
            s2 = Helper.preparationStr( s2, new String[]{".", ",", "-", "\\", "/"} );

            // строка для сравненияы
            String s1;

            // перебираю заголовки месяцев
            for( String[] hMonth : headers_option_months )
            {
                // очищаю строку от лишнего, и в нижний регистр
                s1 = Helper.preparationStr( hMonth[0], new String[]{".", ",", "-", "\\", "/"} );

                // similarity of cells
                if( new JaroWinkler().similarity( s1, s2 ) > Option.RATE_SIMILAR_MONTH )
                {
                    // fixing. the year should consist of two digits
                    cYear = cYear > 2000 ? ( cYear - 2000 ) : cYear;

                    // create a new key for the header
                    String key = hMonth[2] + "_" + cYear;

                    // priority check, and set the row
                    this.checkPriority( s2, cellIterator, hMonth[1], key, Integer.valueOf( hMonth[4] ), Integer.valueOf( hMonth[5] ) );

                    flag = true;
                    break;
                } else flag = false;
            }
        }

        return flag;
    }

    /**
     * Removing characters by pattern
     * @param s - String
     * @return    String
     */
    private String replaceMFT( String s )
    {
        // Patterns for month
        String[] patterns = {
                "1-15",

                "15-28",
                "15-30",
                "15-31",

                "16-28",
                "16-30",
                "16-31"
        };

        for( String pattern : patterns ){
            // remove a pattern from the string
            s = s.replace( pattern, "" );
        }

        // return the clean string
        return s;
    }

    /**
     * Priority check and set
     */
    private void checkPriority( String s2, int i, String type, String key, Integer priority, Integer priority_any )
    {
        // Checking the presence of a header
        if( headerExistByKey( key ) )
        {
            // getting a header from a specific list
            ObjHeader _header = this.getHeaderByKey( key );

            // if a old header exist
            if( _header.name.length() > 0 && _header.key.length() > 0 && _header.type.length() > 0 )
            {
                // If the priority of the old header is less than the new header priority
                if( _header.priority < priority )
                {
                    // deleting an old header by id
                    Phaser.header.remove( _header.itr );

                    // set a new header
                    this._setHeader( i, s2, type, key, priority, priority_any );
                }
                // If this is a month
                else if( type.equals( "month" ) )
                {
                    // Rename the month key for the first half
                    _header.key = _header.key + "_1";

                    Phaser.header.put( _header.itr, _header );

                    // set a new header, with key for second half
                    this._setHeader( i, s2, type, key + "_2", priority, priority_any );
                }
            }
            // set a header
        } else this._setHeader( i, s2, type, key, priority, priority_any );
    }

    /**
     * Internal method.
     * Set a new header to the list.
     */
    private void _setHeader( int i, String s2, String type, String key, Integer priority , Integer priority_any )
    {
        Phaser.header.put( i, new ObjHeader( i, s2, type, key, priority, priority_any ) );

        installed = true;
    }

    /**
     * Определение даты из строки по шаблону
     *
     * @param s - String
     * @return int array
     */
    private int[] _detectedDateByPattern( String s )
    {
        // дата по дефолту
        int[] date = {0, 0};

        // календарь
        Calendar calendar = new GregorianCalendar();

        // шаблоны даты
        String[] pattern = {
                "dd.MM.yy",
                "dd/MM/yy",
                "dd-MM-yy",

                "dd.MM.yyyy",
                "dd/MM/yyyy",
                "dd-MM-yyyy",

                //----------

                "MM.yy",
                "MM/yy",
                "MM-yy",

                "MM.yyyy",
                "MM/yyyy",
                "MM-yyyy",

                "MM,yyyy",

                "MM" // только месяц
        };

        // перебираю шаблоны
        for( String aPattern : pattern )
        {
            try
            {
                // попытка получить дату из строки по шаблону
                Date ptDate = new SimpleDateFormat( aPattern, Locale.ENGLISH ).parse( s );

                //
                calendar.setTime( ptDate );

                date[0] = calendar.get( Calendar.YEAR );
                date[1] = calendar.get( Calendar.MONTH ) + 1;

                break;
            } catch( Exception ignored )
            {
            }
        }

        return date;
    }
}