package Phaser;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.*;

class FtpHandler{
    /**
     * Скачивает файл с FTP.
     *
     * @param fileName - имя файла
     */
    static boolean getGridByName( String fileName ) {
        try {
            // ftp модуль
            FTPClient ftpClient = new FTPClient();

            // подключаюсь к ftp
            ftpClient.connect(Option.FTP_HOST, Option.FTP_PORT);

            // авторизируюсь
            ftpClient.login(Option.FTP_USER, Option.FTP_PASS);

            // кодировка
            ftpClient.setControlEncoding("UTF-8");

            // хз что это, не разобрался
            ftpClient.enterLocalPassiveMode();

            // получаю директорию для записи
            String locDir = Helper.getDir();

            // если директория получена
            if (locDir != null) {
                // куда сохраним файл
                File saveFile = new File(locDir + Option.DIR_SEP + fileName);

                // создаю поток для записи
                OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(saveFile));

                ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

                // попытка скачать файл
                boolean success = ftpClient.retrieveFile(Option.FTP_ROOT + fileName, outputStream);

                // закрываю поток
                outputStream.close();

                // если удалось скачать файл
                if (success) return true;
            }
        } catch (IOException ex) {
            // Уведомление из парсера
            Notification.create(
                    "Exception while loading a file. <b>FtpHandler.getGridByName</b>. Message:" + Helper.getStackTrace(ex),
                    fileName,
                    0
            );
            return false;
        }

        return false;
    }

}
