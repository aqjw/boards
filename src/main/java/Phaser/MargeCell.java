package Phaser;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;

class MargeCell{

    /**
     * Проставление значений для объединенных ячеек
     *
     * @param sheet - лист
     */
    static void unMerge( XSSFSheet sheet ){
        String value = "";
        for( int i = sheet.getNumMergedRegions() - 1; i >= 0; i-- ){
            CellRangeAddress region = sheet.getMergedRegion( i );
            Row firstRow = sheet.getRow( region.getFirstRow() );
            Cell firstCellOfFirstRow = firstRow.getCell( region.getFirstColumn() );

            if( firstCellOfFirstRow.getCellType() == Cell.CELL_TYPE_STRING ){
                value = firstCellOfFirstRow.getStringCellValue();
            }

            sheet.removeMergedRegion( i );

            for( Row row : sheet ){
                for( Cell cell : row ){
                    if( region.isInRange( cell.getRowIndex(), cell.getColumnIndex() ) ){
                        cell.setCellType( Cell.CELL_TYPE_STRING );
                        cell.setCellValue( value );
                    }
                }
            }

        }
    }
}
