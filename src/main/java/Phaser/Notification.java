package Phaser;

public class Notification{

    /**
     * Создание уведомления
     *
     * @param msg - сообщение
     * @param id - ключ файла
     * @param status - статус уведомленния
     */
    public static void create(String msg, String id, Integer status){
        // если сообщение не пусто
        if( msg != null && msg.length() > 0 ){
            RequestsToServer.sendRequestV(
                Option.URL_CREATE_NOTIFY +
                    Helper.createUrlParam(
                        new String[][]{
                            {"msg", msg},
                            {"id", id},
                            {"status", String.valueOf( status )}
                        }
                    )
            );
        }

        System.out.println(msg);
    }

    /**
     * Создание уведомления
     *
     * @param msg - сообщение
     * @param id - ключ файла
     */
    public static void create(String msg, String id){
        // если сообщение не пусто
        if( msg != null && msg.length() > 0 ){
            RequestsToServer.sendRequestV(
                Option.URL_CREATE_NOTIFY +
                    Helper.createUrlParam(
                        new String[][]{
                            {"msg", msg},
                            {"id", id}
                        }
                    )
            );
        }
    }

    /**
     * Создание уведомления
     *
     * @param msg - сообщение
     * @param status - статус уведомленния
     */
    public static void create(String msg, Integer status){
        // если сообщение не пусто
        if( msg != null && msg.length() > 0 ){
            RequestsToServer.sendRequestV(
                Option.URL_CREATE_NOTIFY +
                    Helper.createUrlParam(
                        new String[][]{
                            {"msg", msg},
                            {"status", String.valueOf( status )}
                        }
                    )
            );
        }
        System.out.println(msg);

    }

    /**
     * Создание уведомления
     *
     * @param msg - сообщение
     */
    public static void create(String msg){
        // если сообщение не пусто
        if( msg != null && msg.length() > 0 ){
            RequestsToServer.sendRequestV(
                Option.URL_CREATE_NOTIFY +
                    Helper.createUrlParam(
                        new String[][]{{"msg", msg}}
                    )
            );
        }
    }
}
