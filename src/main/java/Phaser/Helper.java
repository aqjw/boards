package Phaser;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Helper{


    /**
     * Проверяет и создает директорию для файлов.
     */
    static String getDir()
    {
        // Текущая дата
        String nowDate = getDate( "yyyy/MM/dd" );

        // полный путь к директории
        String directory = Option.LOCAL_ROOT + nowDate;

        // путь к директории на локале. Где будут хранится сетки
        File locDir = new File( directory );

        // Если директории не существует
        if( ! locDir.exists() )
        {
            // попытка создать директорию
            try
            {
                locDir.mkdirs();
                return directory;
            } catch( SecurityException ex )
            {
                // Уведомление из парсера
                Notification.create(
                        "An exception when creating a directory. <b>Helper.getDir</b>. Message:" + Helper.getStackTrace( ex ),
                        0
                );
                return null;
            }

            // есил существует
        } else return directory;
    }

    /**
     * Получает разширение файла,
     * из его названия.
     *
     * @param fileName - название или путь
     */
    static String getFileFormat( String fileName )
    {
        // Разбиваю строку по точке
        String[] pieces = fileName.split( "\\." );

        // Если елементов боольше нуля
        if( pieces.length > 0 )
        {
            // Получаю последний елемент массива
            String exp = pieces[pieces.length - 1];

            // Переводу в нижний регистр
            // возвращаю
            return exp.toLowerCase();

            // Иначе вохвращаю пустую строку
        } else return "";
    }

    /**
     * Создание строки с GET параметрами
     *
     * @param params - массив параметров
     * @return String
     */
    static String createUrlParam( String[][] params )
    {
        // строка на ответ
        StringBuilder result = new StringBuilder();

        // перебираю параметры
        for( String[] param : params )
        {
            // разделитель GET параметров
            result.append( result.length() > 0 ? "&" : "?" );

            // GET параметры
            result.append( param[0] ).append( "=" ).append( urlEncode( param[1] ) );
        }

        // добавляю ключ парсера
        return createUrlParam( result.toString() );
    }

    /**
     * Создание строки с GET параметрами
     * Добавление ключа парсера.
     *
     * @param params - строка с параметрами
     * @return String
     */
    static String createUrlParam( String params )
    {
        // строка на ответ
        String result = params;

        // добавляю ключ парсера
        result += result.length() > 0 ? "&" : "?";
        result += "phaser_key=" + Option.PHASER_KEY;

        return result;
    }

    /**
     * Кодирование параметров ссылки
     *
     * @param s - String
     * @return - String
     */
    static String urlEncode( String s )
    {
        // попытка кодировать параметр
        try
        {
            return URLEncoder.encode( s, "UTF-8" );
        } catch( UnsupportedEncodingException e )
        {
            return s;
        }
    }

    /**
     * Проверка строки на наличие цифр
     *
     * @param s - String
     * @return boolean
     */
    static boolean hasInt( String s )
    {
        // перебираю все символы строки
        for( int i = 0; i < s.length(); i++ )
            if( Character.isDigit( s.charAt( i ) ) )
                return true;

        return false;
    }

    /**
     * Проверка строки на наличие букв
     *
     * @param s - String
     * @return boolean
     */
    static boolean hasLetter( String s )
    {
        // перебираю все символы строки
        for( int i = 0; i < s.length(); i++ )
            if( Character.isLetter( s.charAt( i ) ) )
                return true;

        return false;
    }

    /**
     * Возвращает текущею дату в указанном формате
     *
     * @return String
     */
    static String getDate( String pattern )
    {
        return new SimpleDateFormat( pattern ).format( new Date() );
    }

    /**
     * Очищает сроку от лишних символов, и переводит в нижний регистр
     *
     * @param s         - строка
     * @param charUnset - символы очистки
     * @return String
     */
    static String preparationStr( String s, String[] charUnset )
    {

        if( s == null || s.length() == 0 )
            return "";

        // перебирю символы
        for( String aCharUnset : charUnset )
        {
            s = s.replace( aCharUnset, "" );
        }
        // пробелы и в нижний
        return s.trim().toLowerCase();
    }

    /**
     * Получение сообщения исклоючения
     *
     * @return string
     */
    static String getStackTrace( final Throwable throwable )
    {
        final StringWriter sw = new StringWriter();
        final PrintWriter pw = new PrintWriter( sw, true );
        throwable.printStackTrace( pw );
        return sw.getBuffer().toString();
    }

    /**
     * Downloading a file from a URL
     *
     * @param from String
     * @param to   String
     * @return     Boolean
     */
    static boolean downFile( String from, String to )
    {
        try
        {
            URL url = new URL( from );

            InputStream is = url.openStream();
            OutputStream os = new FileOutputStream( to );

            byte[] b = new byte[2048];
            int length;

            while( ( length = is.read( b ) ) != - 1 )
                os.write( b, 0, length );


            is.close();
            os.close();

            return true;

        } catch( Exception ex )
        {
            // Уведомление из парсера
            Notification.create(
                    "Exception when download file from URL. <b>Helper.downFile</b>. Message:" + Helper.getStackTrace( ex ),
                    0
            );
        }

        return false;
    }
}
