package Phaser;


import info.debatty.java.stringsimilarity.JaroWinkler;

class Employment{


    // для типов занятости
    private String[][] employment_option;

    // для легенд
    private String[][] legends;


    Employment( String fileKey, Boolean employmentHand )
    {
        // получаю легенды занятости
        legends = RequestsToServer.getLegends( Integer.parseInt( fileKey ) );

        // получаю варианты занятости
        employment_option = RequestsToServer.getEmployment( fileKey, employmentHand );
    }

    /**
     * Форматирование строки цвета
     */
    private String formatColor( String color )
    {
        // Если цвет пуст, по умолчанию это белый
        color = color == null ? "" : color;

        color = color.length() > 6 ? color.substring( 2 ) : color;

        // на случай если в цвете будет мельше 6-ти символов
        color = color.length() < 6 ? "ffffff" : color;

        // удаляю пробелы и в нижний регистр
        return color.trim().toLowerCase();
    }

    // Определение занятости из строки (занято/свободно/резерв...)
    // Определение заняточки по цвету (по заданной легенде)
    String colorDetermination( String s2, String colorHEX )
    {
        // занятость по умолчанию - занято
        String res = Option.EMP_BUSY;

        // Форматирвание стркои цвета
        colorHEX = formatColor( colorHEX );

        // если строка не пустая
        if( s2.trim().length() > 0 )
        {
            // Класс для сравнивания строк
            JaroWinkler jw = new JaroWinkler();

            // значение ячейки, удаляю провелы и в нижний регистр
            s2 = s2.trim().toLowerCase();

            // Временные еременные для имени, статуса
            String s1, status;

            // перебираю все варианты занятостей
            for( String[] option : employment_option )
            {
                // получаю тип сравнения
                int compar = Integer.parseInt( option[2] );

                // получаю имя для сравнения
                s1 = option[0].toLowerCase();

                // получаю статус занятости
                status = option[1];

                // определяю тип сравнения,
                // и провожу стравнение
                switch( compar )
                {
                    case Option.COMPARISON_TYPE__EQUALS:
                        if( s2.equals( s1 ) ) continue;
                        break;
                    case Option.COMPARISON_TYPE__INDEX_OF:
                        if( s2.contains( s1 ) ) continue;
                        break;
                    case Option.COMPARISON_TYPE__SIMILAR:
                        if( jw.similarity( s1, s2 ) > Option.RATE_SIMILAR ) continue;
                        break;
                    case Option.COMPARISON_TYPE_EQUALS:
                        if( s2.equals( s1 ) ) res = status;
                        break;
                    case Option.COMPARISON_TYPE_INDEX_OF:
                        if( s2.contains( s1 ) ) res = status;
                        break;
                    case Option.COMPARISON_TYPE_SIMILAR:
                        if( jw.similarity( s1, s2 ) > Option.RATE_SIMILAR ) res = status;
                        break;
                }
            }

            /*
              если строка пустая,
              будет определять занятость по легендам
             */
        } else
        {
            // перебираю легенды
            for( String[] legend : legends )
            {
                // сравниваю легенду с цветом ячейки
                if( legend[1].equals( colorHEX ) )
                {
                    // статус ячейки по легенде
                    res = legend[0];
                }
            }
        }

        return res;
    }
}