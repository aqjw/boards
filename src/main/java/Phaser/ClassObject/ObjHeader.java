package Phaser.ClassObject;

/**
 * Создание объектов заголовка
 */
public class ObjHeader{
    public Integer itr, priority, priority_any;
    public String name, type, key;

    public ObjHeader( Integer itr, String name, String type, String key, Integer priority , Integer priority_any ){
        this.itr = itr;
        this.name = name;
        this.type = type;
        this.key = key;
        this.priority = priority;
        this.priority_any = priority_any;
    }
}
