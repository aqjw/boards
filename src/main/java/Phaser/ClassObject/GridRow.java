package Phaser.ClassObject;

/**
 * Создние объекта сроки
 */
public class GridRow{


    public String type, value, key;

    public GridRow( String type, String value, String key )
    {
        this.key = key;
        this.type = type;
        this.value = value;
    }

    public GridRow( String type, int value, String key )
    {
        this.key = key;
        this.type = type;
        this.value = String.valueOf( value );
    }

    public void print()
    {
        System.out.println( key + " == " + value );
    }
}
