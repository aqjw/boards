package Phaser;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class RequestsToServer{


    /**
     * Цикл, проверка наличие сеток для парсера
     */
    public static void gridReader()
    {
        new Checker().run();
        new Command().run();
    }

    /**
     * Запрос на сервер, возвращает массив
     */
    static JSONArray sendRequest( String url )
    {
        JSONArray json_arr = null;
        try
        {

            // формирую запрос
            HttpURLConnection connection = ( HttpURLConnection ) new URL( url ).openConnection();

            // метод
            connection.setRequestMethod( "GET" );

            // получаю результат запроса
            BufferedReader in = new BufferedReader( new InputStreamReader( connection.getInputStream() ) );

            // декодирую из json в массив
            json_arr = new JSONArray( in.readLine() );

            // закрываю буфер
            in.close();

        } catch( Exception ignored )
        {
        }

        return json_arr;
    }


    /**
     * Запрос на сервер, ничего не возвращает
     */
    static void sendRequestV( String url )
    {
        try
        {
            // create request
            HttpURLConnection connection = ( HttpURLConnection ) new URL( url ).openConnection();

            // method
            connection.setRequestMethod( "GET" );

            // send
            connection.getInputStream();
        } catch( Exception ignored )
        {
        }
    }

    /**
     * Проверяю наличие сеток
     */
    static String[] getGrid()
    {
        // для названия сетки, и ключа
        String[] result = {null, null, null, null, null};

        try
        {
            // формирую запрос
            URL obj = new URL( Option.URL_GET_GRID + Helper.createUrlParam( "" ) );
            HttpURLConnection connection = ( HttpURLConnection ) obj.openConnection();

            // метод
            connection.setRequestMethod( "GET" );

            // получаю результат запроса
            BufferedReader in = new BufferedReader( new InputStreamReader( connection.getInputStream() ) );
            String json = in.readLine();

            // декодирую из json
            JSONObject jsonObj = new JSONObject( json );

            // статус запроса
            Boolean status = jsonObj.getBoolean( "status" );

            // если сетка найдена
            if( status )
            {
                // название файла сетки
                result[0] = jsonObj.getString( "file" );

                // получаю id файла
                result[1] = jsonObj.getString( "id" );

                // get file name
                result[2] = jsonObj.getString( "name" );

                // get the number of the header
                result[3] = jsonObj.getString( "has_headers" );

                // get the number of the employment
                result[4] = jsonObj.getString( "hand_employment" );
            }

            // закрываю буфер
            in.close();

        } catch( Exception ex )
        {
            // Уведомление из парсера
            Notification.create(
                    "Exception when checking the grid on a server. <b>Request.getGrid</b>. Message:" + Helper.getStackTrace( ex ),
                    0
            );
        }

        return result;
    }

    /**
     * Получаю варианты занятости
     */
    static String[][] getEmployment( String fileKey, Boolean employmentHand )
    {
        // определяю массив
        String[][] employment = {};

        String params = "";
        // if this grid has the employment of manually
        if( employmentHand ){
            params = "?fileKey=" + fileKey;
        }

        try
        {
            // получаю json
            // декодирую из json в массив
            JSONArray js_arr = sendRequest( Option.URL_GET_EMPLOYMENT + Helper.createUrlParam( params ) );

            // массив для занятостей
            employment = new String[js_arr.length()][3];

            // перебираю все занятости
            for( int i = 0; i < js_arr.length(); i++ )
            {
                // массив в объект
                JSONObject js_obj = js_arr.getJSONObject( i );

                // получаю название для стравнения
                employment[i][0] = String.valueOf( js_obj.getString( "name" ) );
                // получаю статус
                employment[i][1] = String.valueOf( js_obj.getString( "status" ) );
                // получаю тип сравнения
                employment[i][2] = String.valueOf( js_obj.getString( "compar" ) );
            }


        } catch( Exception ex )
        {
            // Уведомление из парсера
            Notification.create(
                    "Exception when getting 'employments' from a server. <b>Request.getEmployment</b>. Message:" + ex.getMessage(),
                    0
            );
        }

        return employment;
    }

    /**
     * Получаю заголовки для определения столбцев
     */
    static String[][] getHeaders( boolean months )
    {
        // определяю массив
        String[][] headers = {};

        // если нужно получить только месяцы
        String months_param = months ? "?months" : "";

        try
        {
            // получаю json
            // декодирую из json в массив
            JSONArray js_arr = sendRequest( Option.URL_GET_HEADERS + Helper.createUrlParam( months_param ) );

            // массив для заголовков
            headers = new String[js_arr.length()][6];

            // перебираю все заголовки
            for( int i = 0; i < js_arr.length(); i++ )
            {
                // массив в объект
                JSONObject js_obj = js_arr.getJSONObject( i );

                // получаю название для стравнения
                headers[i][0] = String.valueOf( js_obj.getString( "name" ) );
                // получаю тип
                headers[i][1] = String.valueOf( js_obj.getString( "type" ) );
                // получаю ключ
                headers[i][2] = String.valueOf( js_obj.getString( "key" ) );
                // получаю тип сравнения
                headers[i][3] = String.valueOf( js_obj.getString( "compar" ) );
                // получаю приоритет колонок для переопределения
                headers[i][4] = String.valueOf( js_obj.getString( "priority" ) );
                // получаю приоритет для остального
                headers[i][5] = String.valueOf( js_obj.getString( "priority_any" ) );
            }

        } catch( Exception ex )
        {
            // Уведомление из парсера
            Notification.create(
                    "Exception when getting 'headers' from a server. <b>Request.getHeaders</b>. Message:" + ex.getMessage(),
                    0
            );
        }

        return headers;
    }

    /**
     * Получаю заголовки для определения столбцев.
     * Заголовки которые проставлены вручную.
     */
    static String[][] getHeaders( String fileKey )
    {
        // определяю массив
        String[][] headers = {};

        try
        {
            // получаю json
            // декодирую из json в массив
            JSONArray js_arr = sendRequest( Option.URL_GET_HEADERS + Helper.createUrlParam( "?fileKey=" + fileKey ) );

            // массив для заголовков
            headers = new String[js_arr.length()][6];

            // перебираю все заголовки
            for( int i = 0; i < js_arr.length(); i++ )
            {
                // массив в объект
                JSONObject js_obj = js_arr.getJSONObject( i );

                // получаю название для стравнения
                headers[i][0] = String.valueOf( js_obj.getString( "name" ) );
                // получаю тип
                headers[i][1] = String.valueOf( js_obj.getString( "type" ) );
                // получаю ключ
                headers[i][2] = String.valueOf( js_obj.getString( "key" ) );
                // получаю тип сравнения
                headers[i][3] = String.valueOf( js_obj.getString( "compar" ) );
                // получаю приоритет колонок для переопределения
                headers[i][4] = String.valueOf( js_obj.getString( "priority" ) );
                // получаю приоритет для остального
                headers[i][5] = String.valueOf( js_obj.getString( "priority_any" ) );
            }

        } catch( Exception ex )
        {
            // Уведомление из парсера
            Notification.create(
                    "Exception when getting 'headers' from a server. <b>Request.getHeaders</b>. Message:" + ex.getMessage(),
                    0
            );
        }

        return headers;
    }

    /**
     * Получаю легенды по id сетки
     */
    static String[][] getLegends( int grid_id )
    {
        // определяю массив
        String[][] legends = {};

        try
        {
            // получаю json
            // декодирую из json в массив
            JSONArray js_arr = sendRequest( Option.URL_GET_LEGENDS + Helper.createUrlParam( "?grid_id=" + grid_id ) );

            // массив для легенд
            legends = new String[js_arr.length()][4];

            // перебираю все легенды
            for( int i = 0; i < js_arr.length(); i++ )
            {
                // массив в объект
                JSONObject js_obj = js_arr.getJSONObject( i );

                // получаю статус
                legends[i][0] = String.valueOf( js_obj.getString( "status" ) );
                // получаю цвет
                legends[i][1] = String.valueOf( js_obj.getString( "color" ) );
            }

        } catch( Exception ex )
        {
            // Уведомление из парсера
            Notification.create(
                    "Exception when getting 'legends' from a server. <b>Request.getLegends</b>. Message:" + ex.getMessage(),
                    0
            );
        }

        return legends;
    }

    /**
     * Get a list of cities
     */
    public static String[][] getCities()
    {
        // определяю массив
        String[][] cities = {};

        try
        {
            // get json
            // json decoding
            JSONArray js_arr = sendRequest( Option.URL_GET_CITIES + Helper.createUrlParam( "" ) );

            // array for cities
            cities = new String[js_arr.length()][5];

            for( int i = 0; i < js_arr.length(); i++ )
            {
                // array to object
                JSONObject js_obj = js_arr.getJSONObject( i );

                // get city identifier
                cities[i][0] = String.valueOf( js_obj.getString( "id" ) );

                // get the name of the city
                cities[i][1] = String.valueOf( js_obj.getString( "name" ) );

                // get the priority of the city
                cities[i][2] = String.valueOf( js_obj.getString( "priority" ) );

                // get the region of the city
                cities[i][3] = String.valueOf( js_obj.getString( "region" ) );

                // get the short name of the city
                cities[i][4] = String.valueOf( js_obj.getString( "short" ) );
            }


        } catch( Exception ex )
        {
            // create a notification
            Notification.create(
                    "Exception when getting 'cities' from a server. <b>Request.getCities</b>. Message:" + ex.getMessage(),
                    0
            );
        }

        return cities;
    }

    /**
     * Get a list of regions
     */
    public static String[][] getRegions()
    {
        // определяю массив
        String[][] regions = {};

        try
        {
            // get json
            // json decoding
            JSONArray js_arr = sendRequest( Option.URL_GET_REGIONS + Helper.createUrlParam( "" ) );

            // array for regions
            regions = new String[js_arr.length()][2];

            for( int i = 0; i < js_arr.length(); i++ )
            {
                // array to object
                JSONObject js_obj = js_arr.getJSONObject( i );

                // get region identifier
                regions[i][0] = String.valueOf( js_obj.getString( "id" ) );
                // get the name of the region
                regions[i][1] = String.valueOf( js_obj.getString( "name" ) );
            }


        } catch( Exception ex )
        {
            // create a notification
            Notification.create(
                    "Exception when getting 'regions' from a server. <b>Request.getRegions</b>. Message:" + ex.getMessage(),
                    0
            );
        }

        return regions;
    }

    /**
     * Get all options
     */
    static String[][] getAllOption()
    {
        // определяю массив
        String[][] options = {};

        try
        {
            // get json
            // json decoding
            JSONArray js_arr = sendRequest( Option.URL_GET_ALL_OPTIONS + Helper.createUrlParam( "" ) );

            // array for regions
            options = new String[js_arr.length()][3];

            for( int i = 0; i < js_arr.length(); i++ )
            {
                // array to object
                JSONObject js_obj = js_arr.getJSONObject( i );

                // get key
                options[i][0] = String.valueOf( js_obj.getString( "key" ) );
                // get value
                options[i][1] = String.valueOf( js_obj.getString( "value" ) );
                // get type
                options[i][2] = String.valueOf( js_obj.getString( "type" ) );
            }


        } catch( Exception ex )
        {
            // create a notification
            Notification.create(
                    "Exception when getting 'options' from a server. <b>Request.getAllOption</b>. Message:" + ex.getMessage(),
                    0
            );
        }

        return options;
    }

    /**
     * Get firm id by filekey
     */
    public static int getFirmId( int fileKey )
    {
        // define the firm id
        int id = 0;

        try
        {
            // get json
            // json decoding
            JSONArray js_arr = sendRequest( Option.URL_GET_FIRM_ID + Helper.createUrlParam( "?fileKey=" + fileKey ) );

            // get id from array
            id = js_arr.getJSONObject( 0 ).getInt( "id" );

        } catch( Exception ex )
        {
            // create a notification
            Notification.create(
                    "Exception when getting 'firm id' from a server. <b>Request.getFirmId</b>. Message:" + ex.getMessage(),
                    0
            );
        }

        return id;
    }

    /**
     * Get bords by firm id
     */
    public static String[][] getBordsByFirm( int firmId )
    {
        // определяю массив
        String[][] bords = {};

        try
        {
            // get json
            // json decoding
            JSONArray js_arr = sendRequest( Option.URL_GET_BORDS + Helper.createUrlParam( "?firm=" + firmId ) );

            // array for regions
            bords = new String[js_arr.length()][4];

            for( int i = 0; i < js_arr.length(); i++ )
            {
                // array to object
                JSONObject js_obj = js_arr.getJSONObject( i );

                // get side
                bords[i][0] = js_obj.getString( "side" );
                // get code
                bords[i][1] = js_obj.getString( "code" );
                // get code
                bords[i][2] = js_obj.getString( "type" );
                // get id
                bords[i][3] = js_obj.getString( "id" );
            }

        } catch( Exception ex )
        {
            // create a notification
            Notification.create(
                    "Exception when getting 'bords' from a server. <b>Request.getBordsByFirm</b>. Message:" + ex.getMessage(),
                    0
            );
        }

        return bords;
    }

    /**
     * Get a list of types
     */
    public static String[][] getTypes()
    {
        // array default
        String[][] types = {};

        try
        {
            // get json
            // json decoding
            JSONArray js_arr = sendRequest( Option.URL_GET_TYPES + Helper.createUrlParam( "" ) );

            // array for types
            types = new String[js_arr.length()][2];

            for( int i = 0; i < js_arr.length(); i++ )
            {
                // array to object
                JSONObject js_obj = js_arr.getJSONObject( i );

                // get name
                types[i][0] = String.valueOf( js_obj.getString( "name" ) );

                // get type
                types[i][1] = String.valueOf( js_obj.getString( "type" ) );
            }


        } catch( Exception ex )
        {
            // create a notification
            Notification.create(
                    "Exception when getting 'types' from a server. <b>Request.getTypes</b>. Message:" + ex.getMessage(),
                    0
            );
        }

        return types;
    }

    /**
     * Get a default type
     */
    public static String getDefaultType( String fileKey )
    {
        // default
        String type = "";

        try
        {
            // get json
            // json decoding
            JSONArray js_arr = sendRequest( Option.URL_GET_DEFAULT_TYPE + Helper.createUrlParam( "?fileKey=" + fileKey ) );

            // get type from array
            type = js_arr.getJSONObject( 0 ).getString( "type" );

        } catch( Exception ex )
        {
            // create a notification
            Notification.create(
                    "Exception when getting 'default type' from a server. <b>Request.getDefaultType</b>. Message:" + ex.getMessage(),
                    0
            );
        }

        return type;
    }

    /**
     * Set row
     */
    public static void setRow( String rows )
    {
        try
        {
            // created an object to send request
            CloseableHttpClient httpClient = HttpClientBuilder.create().build();

            // created an object to build json
            JSONObject json = new JSONObject();
            // param 'rows' - json
            json.put( "rows", rows );
            // key of the parser
            json.put( "phaser_key", Option.PHASER_KEY );

            // create request. set url
            HttpPost request = new HttpPost( Option.URL_SET_ROW );

            // set params
            StringEntity params = new StringEntity( json.toString(), "UTF-8" );

            // set header
            request.addHeader( "content-type", "application/json" );


            request.setEntity( params );
            httpClient.execute( request );

            httpClient.close();

        } catch( Exception ex )
        {
            // create a notification
            Notification.create(
                    "Exception when sending a packet of rows to a server. <b>Request.setRow</b>. Message:" + ex.getMessage(),
                    0
            );
        }
    }

}