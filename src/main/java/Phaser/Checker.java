package Phaser;

import java.util.*;

public class Checker extends TimerTask{


    @Override
    public void run()
    {
        completeTask();
    }

    private void completeTask()
    {
        TimerTask task = new TimerTask(){
            public void run()
            {
                // get the grid from the server
                String[] grid = RequestsToServer.getGrid();

                // if there is a grid
                if( grid[0] != null )
                {
                    // try to load a grid by name
                    boolean down = FtpHandler.getGridByName( grid[0] );

                    // if the grid is loaded
                    if( down )
                    {
                        // run parser
                        new Phaser().run( grid );
                    }
                }
            }
        };

        // timer for grid check
        new Timer()
                .schedule( task, Option.TIMER_TASK, Option.TIMER_TASK );
    }
}