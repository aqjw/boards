package Phaser.Handlers;

import Phaser.ClassObject.ObjHeader;
import Phaser.RequestsToServer;
import java.util.Map;

public class City{


    // list of regions
    private String[][] regions;

    // list of cities
    private String[][] cities;

    // file name and sheet
    private String fileName_sheetName;

    // the header object
    private final Map<Integer, ObjHeader> header;

    public City( String fileName_sheetName, Map<Integer, ObjHeader> header )
    {
        this.fileName_sheetName = fileName_sheetName.toLowerCase();
        this.header = header;

        // get regions
        regions = RequestsToServer.getRegions();

        // get cities
        cities = RequestsToServer.getCities();
    }

    /**
     * Get a region id
     */
    public Integer[] getRegion( String value, Integer[] city_region )
    {
        value = value.toLowerCase();

        if( value.trim().length() > 0 )
        {
            for( String[] _region : regions )
            {
                // divided the region into ru and ua
                String[] _regions = _region[1].split( "\\|\\|" );

                for( String region : _regions )
                {
                    region = region.toLowerCase();

                    // find the name of the region
                    if( value.contains( region ) )
                    {
                        // region ID
                        city_region[2] = Integer.parseInt( _region[0] );
                    }
                }
            }
        }

        return city_region;
    }

    /**
     * Get a city id
     */
    public Integer[] getCity( String value, Boolean in_names, Integer[] city_region )
    {
        value = value.toLowerCase();

        if( value.trim().length() > 0 )
        {
            for( String[] _city : cities )
            {
                // divided the city into ru and ua
                String[] _cities = _city[1].split( "\\|\\|" );

                // divided the short city name
                String[] _short_cities = _city[4].split( "\\|\\|" );

                int i = 0;
                String short_city = "";

                for( String city : _cities )
                {
                    city = city.toLowerCase();

                    if( _short_cities.length > i ){
                        short_city = _short_cities[i].toLowerCase();
                    }
                    short_city = short_city.length() == 0 ? city : short_city;

                    // check city
                    if( Integer.parseInt( _city[3] ) == city_region[2] || city_region[2] == 0 )
                    {
                        // if "in_name" is true, find the name of the city in the name of the file and the sheet
                        if( in_names )
                        {
                            // find the name of the city. Check priority
                            if( fileName_sheetName.contains( value ) && ( city_region[1] < Integer.parseInt( _city[2] ) ) )
                            {
                                // city ID and priority
                                city_region[0] = Integer.parseInt( _city[0] );
                                city_region[1] = Integer.parseInt( _city[2] );
                            }
                        } else
                        {
                            // find the name of the city
                            if( ( value.contains( city ) || value.contains( short_city ) ) && ( city_region[1] < Integer.parseInt( _city[2] ) ) )
                            {
                                // city ID and priority
                                city_region[0] = Integer.parseInt( _city[0] );
                                city_region[1] = Integer.parseInt( _city[2] );
                            }
                        }
                    }

                    i++;
                }
            }
        }

        return city_region;
    }

    /**
     * Get priority_any from header (region)
     */
    public Integer getPriority( String key, Integer priority_cell )
    {
        for( Map.Entry<Integer, ObjHeader> head : header.entrySet() )
        {
            // if priority_any > priority cell
            if( key.equals( head.getValue().key ) && priority_cell < head.getValue().priority_any )
            {
                // return the priority_any
                return head.getValue().priority_any;
            }
        }

        return priority_cell;
    }
}
