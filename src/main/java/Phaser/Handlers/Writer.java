package Phaser.Handlers;

import Phaser.Option;
import Phaser.Phaser;
import Phaser.RequestsToServer;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Writer{


    // all rows
    private int all_rows = 0;

    // processed rows
    private int processed_rows = 0;

    // firm id
    private int firmId;

    // the row counter in the package
    private int amount_package = 0;

    // List of Rows
    private List<String> _package = new ArrayList<>();

    // Object "Gson", for create json
    private Gson gson = new Gson();

    /**
     * bords list
     *
     * [i][0] - side
     * [i][1] - code
     * [i][2] - type
     * [i][3] - id
     */
    private String[][] bords;

    public Writer( String fileKey )
    {
        this.run( Integer.parseInt( fileKey ) );
    }

    private void run( int fileKey )
    {
        // get firm id by file key
        firmId = RequestsToServer.getFirmId( fileKey );

        // get bords
        bords = RequestsToServer.getBordsByFirm( firmId );
    }

    /**
     * Get the row id
     *
     * @return int - id row in the table
     */
    private int getRowId( String side, String code, String type )
    {
        // if all the parameters are null
        if( side == null && type == null )
        {
            return 0;
        }

        boolean _side = false,
                _code = false,
                _type = false;

        for( String[] bord : bords )
        {
            // checking side
            if( side != null )
            {
                if( side.equals( bord[0] ) )
                {
                    _side = true;
                }

            } else _side = true;

            // checking code
            if( code.equals( bord[1] ) )
            {
                _code = true;
            }

            // checking type
            if( type != null )
            {
                if( type.equals( bord[2] ) )
                {
                    _type = true;
                }

            } else _type = true;


            // if all the fields are true
            if( _side && _code && _type )
            {
                /*
                 * row is exist in the table,
                 * returns its id
                 */
                return Integer.parseInt( bord[3] );
            }

            _side = false;
            _code = false;
            _type = false;
        }

        // is new row
        return 0;
    }

    /**
     * Write the row in the table
     */
    public void write( Map<String, String> row )
    {
        String
                side = row.get( "side" ),
                code = row.get( "code" ),
                type = row.get( "type" );

        // if the code is not null
        if( code != null )
        {
            // get the row id by parameters
            int row_id = this.getRowId( side, code, type );

            // add the row id to the row
            row.put( "id", String.valueOf( row_id ) );

            // add the firm id to the row
            row.put( "firm", String.valueOf( firmId ) );

            // add the file id to the row
            row.put( "fileKey", String.valueOf( Phaser.fileKey ) );

            // Convert the list to json
            String json_row = gson.toJson( row );

            // row to sender
            sender( json_row );

            processed_rows++;
        }

        all_rows++;
    }

    /**
     * Checker and sender
     * @param json_row
     */
    private void sender( String json_row )
    {
        amount_package++;

        // if string is not null
        if( json_row.length() > 0 )
        {
            // add the string to the package
            _package.add( json_row );
        }

        // if amount the rows in package equal SEND_LIMIT
        if( amount_package >= Option.SEND_PACKAGE_LIMIT )
        {
            System.out.println("sending package: " + amount_package);

            String json = gson.toJson( _package );
            // new thread to sender the row to the server
            new Thread( () ->
                    RequestsToServer.setRow( json )
            ).start();

            // reset the package
            _package.clear();
            amount_package = 0;
        }
    }

    /**
     * Send package.
     * The last rows in the sheet
     */
    public void lastRow()
    {
        //
        amount_package = Option.SEND_PACKAGE_LIMIT;
        sender("");
    }

    public int getTotal()
    {
        return all_rows;
    }
    public int getProcessed()
    {
        return processed_rows;
    }
}
