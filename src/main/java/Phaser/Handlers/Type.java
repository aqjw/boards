package Phaser.Handlers;

import Phaser.ClassObject.GridRow;
import Phaser.RequestsToServer;

public class Type{

    /*
     * list of types
     *
     * [i][0] - name
     * [i][1] - type
     */
    private String[][] types = null;

    // default type
    private String default_ = null;

    // file key
    private String fileKey;

    public Type( String fileKey )
    {
        this.fileKey = fileKey;
    }

    public String find( String value )
    {
        if( value == null )
            return "";

        // check and get types
        getTypes();

        // preparation value
        value = value.toLowerCase().trim();

        for( String[] type : types)
        {
            // preparation the type
            String _type = type[0].toLowerCase().trim();

            // if the value is equal to type
            if( value.contains( _type ) )
            {
                // return type
                return type[1];
            }
        }

        // undefined
        return "";
    }

    /**
     * Find the type in the string
     * @param gridRow - row
     * @return String - type
     */
    public String find( GridRow[] gridRow, String fileSheet_name )
    {
        // check and get types
        getTypes();

        String value = "";

        for( GridRow cell : gridRow )
        {
            if( cell == null || cell.type.equals( "month" ) )
                continue;

            // find the type in the value
            value = find( cell.value );

            // if the length of the value is greater than zero
            if( value.length() > 0 ){
                // stop the cycle
                break;
            }
        }

        // if the type is not defined
        if( value.length() == 0 )
        {
            // find the type in the file and sheet name
            value = find( fileSheet_name );
        }

        // if the type is not defined, set the default type
        value = value.length() > 0 ? value : default_;

        return value;
    }

    /**
     * Get types
     */
    private void getTypes()
    {
        // if types are null
        if( types == null )
        {
            // get types
            types = RequestsToServer.getTypes();
        }

        // if the default type is null
        if( default_ == null )
        {
            // get the default type
            default_ = RequestsToServer.getDefaultType( fileKey );
        }
    }

    /**
     * Find the type in row
     *
     * @param gridRow - row
     * @return boolean
     */
    public boolean isExist( GridRow[] gridRow )
    {
        for( GridRow cell : gridRow )
        {
            if( cell == null )
                continue;

            // if the key of the cell is equal to the "type"
            if( cell.key.equals( "type" ) )
            {
                // type is exist.
                // check, type is empty
                return cell.value.length() > 0;
            }
        }

        // type is not exist
        return false;
    }

}
