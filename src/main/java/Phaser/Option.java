package Phaser;


public class Option{


    // Ключ парсера
    final static String PHASER_KEY = "3FG2oSNm1f";

    // задержка в милисекундах, между запросами на проверку наличия сеток
    final static int TIMER_TASK = 3000;

    // лимит. максимальный пакет готовый строк, для отправки на сервер
    public final static int SEND_PACKAGE_LIMIT = 50;

    // status busy
    final static String EMP_BUSY = "busy";

    /**
     * Параметры для подключение к FTP.
     */
    final static String FTP_HOST = "";
    final static String FTP_USER = "";
    final static String FTP_PASS = "";
    final static Integer FTP_PORT = 21;

    // путь к файлам на ftp
    final static String FTP_ROOT = "/web/devboards.praid.com.ua/public_html/storage/uploads/";

    // путь к файлам на локале
    final static String LOCAL_ROOT = "C:/phaser/";

    // Разделитель директорий
    final static String DIR_SEP = "/";


    // ссылка на получение заголовков сетки
    final static String URL_GET_HEADERS = "http://devboards.praid.com.ua/get_headers";

    // ссылка на получение легенд сетки
    final static String URL_GET_LEGENDS = "http://devboards.praid.com.ua/get_legends";

    // ссылка на получение занятостей сетки
    final static String URL_GET_EMPLOYMENT = "http://devboards.praid.com.ua/get_employment";

    // ссылка на получение сетки
    final static String URL_GET_GRID = "http://devboards.praid.com.ua/get_grid";

    // ссылка для получение регионов
    final static String URL_GET_REGIONS = "http://devboards.praid.com.ua/get_regions";

    // ссылка для получение городов
    final static String URL_GET_CITIES = "http://devboards.praid.com.ua/get_cities";

    // ссылка на получение id фирмы
    final static String URL_GET_FIRM_ID = "http://devboards.praid.com.ua/get_firm_id";

    // ссылка на получение плоскостей по id фирмы
    final static String URL_GET_BORDS = "http://devboards.praid.com.ua/get_bords";

    // ссылка на получение типов плоскости
    final static String URL_GET_TYPES = "http://devboards.praid.com.ua/get_types";

    // ссылка на получение все опций парсера
    final static String URL_GET_ALL_OPTIONS = "http://devboards.praid.com.ua/get_all_options";

    // ссылка на получение типа по умолчанию
    final static String URL_GET_DEFAULT_TYPE = "http://devboards.praid.com.ua/get_default_type";

    // ссылка для записи строки в бд
    final static String URL_SET_ROW = "http://devboards.praid.com.ua/set_row";

    // ссылка для создания уведомлений
    final static String URL_CREATE_NOTIFY = "http://devboards.praid.com.ua/create_notification";


    // Минимальный порог вхождения, елементов заголовка.
    final static int HEADER_MINIMUM = 3;

    // Минимальный порох пустых ячеек в строке
    final static int ROW_MINIMUM_NULL_VALUE = 4;

    // Минимальный коэффициент для прохождения, при сравнении строк заголовка
    final static Double RATE_SIMILAR = 0.9;

    // Минимальный коэффициент для прохождения, при сравнении строк заголовка. месяцы
    final static Double RATE_SIMILAR_MONTH = 0.9;

    // types of comparisons
    final static int COMPARISON_TYPE__EQUALS = - 1;
    final static int COMPARISON_TYPE__INDEX_OF = - 2;
    final static int COMPARISON_TYPE__SIMILAR = - 3;
    final static int COMPARISON_TYPE_EQUALS = 1;
    final static int COMPARISON_TYPE_INDEX_OF = 2;
    final static int COMPARISON_TYPE_SIMILAR = 3;

    // list for options
    private String[][] options;

    /**
     * Get all options from server
     */
    void getAllOptionFromServer()
    {
        options = RequestsToServer.getAllOption();
    }

    /**
     * Get option by key.
     * Type option the "list"
     *
     * @return String[]
     */
    String[] getOption( String key )
    {
        // list for return
        String[] list = {};

        for( String[] option : options )
        {
            // if the key matched the required
            if( option[0].equals( key ) )
            {
                // split by comma
                list = option[1].split( "," );
            }
        }

        return list;
    }

    /**
     * Get option by key
     * Type option the "string"

     * @return String
     */
    String getOptionS( String key)
    {
        for( String[] option : options )
        {
            // if the key matched the required
            if( option[0].equals( key ) )
                return option[1];
        }

        return "";
    }

}
