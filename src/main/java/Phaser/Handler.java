package Phaser;

import Phaser.ClassObject.GridRow;
import Phaser.ClassObject.ObjHeader;
import Phaser.Handlers.City;
import Phaser.Handlers.Type;
import Phaser.Handlers.Writer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Handler extends Thread{


    // list for an array of rows
    private List<GridRow[]> gridRows;

    // file key
    private String fileKey;

    private String fileSheet_Name;

    // sheet of number
    private Integer sheetAmount;

    // current sheet
    private Integer cSheet;

    // the city object
    private City cCity;

    // Writer to db
    private Writer wr;

    // Handler the type
    private Type type_;

    //
    private static Option opt = new Option();

    /**
     * Construct
     *
     * @param gridRows    список сеток
     * @param fileKey     ключ файла
     * @param sheetAmount общее количество листов
     * @param cSheet      текущий лист
     */
    Handler( List<GridRow[]> gridRows, String fileKey, Integer sheetAmount, Integer cSheet, String fileName, String sheetName, Map<Integer, ObjHeader> header )
    {
        this.gridRows = gridRows;
        this.fileKey = fileKey;
        this.sheetAmount = sheetAmount;
        this.cSheet = cSheet + 1;

        fileSheet_Name = fileName.concat( sheetName );
        cCity = new City( fileSheet_Name, header );

        // get options from server
        opt.getAllOptionFromServer();
    }


    /**
     * Running the handler
     */
    public void run()
    {
        // if there are more than one rows
        if( gridRows.size() > 0 )
        {
            // run writer
            wr = new Writer( fileKey );

            // handler type
            type_ = new Type( fileKey );

            // run the handler
            this.handler();
        }
    }

    /**
     * The handler.
     * Upgrading information in the rows
     */
    private void handler()
    {
        // current row
        Map<String, String> cRow = new HashMap<>();

        try
        {
            /*
             * city_region of the row
             *
             * [0] - city
             * [1] - priority_city
             * [2] - region
             * [3] - priority_all( only for the region )
             */
            Integer[] city_region;

            // loop.rows
            for( GridRow[] gridRow : gridRows )
            {
                // reset
                city_region = new Integer[]{0, 0, 0, 0};

                // перебираю ячейки
                for( GridRow cell : gridRow )
                {
                    // check cell for existed
                    if( cell != null )
                    {
                        // checking cell for value
                        if( cell.value.length() > 0 )
                        {
                            String value = cell.value;

                            // find cell type
                            switch( cell.type )
                            {
                                case "hyperlink":

                                    value = this.handlerHyperlink( value );

                                    break;

                                case "month":

                                    value = this.handlerMonth( value );

                                    break;

                                case "city":

                                    // get city identifier and priority
                                    city_region = this.handlerCity( value, false, city_region, cell.key );

                                    break;
                            }

                            // add the cell to row
                            cRow.put( cell.key, value );
                        }
                    }
                }

                // if the city is zero, try again
                if( city_region[0] == 0 )
                {
                    // get city identifier and priority
                    city_region = this.handlerCity( "", true, city_region, "" );
                }

                // checking the existence of a type
                if( ! type_.isExist( gridRow ) )
                {
                    // add the type
                    cRow.put( "type", type_.find( gridRow, fileSheet_Name ) );
                }

                // add the city id to the row
                cRow.put( "city", cityCheck( city_region[0] ) );

                // send a row to a table
                wr.write( cRow );

                Thread.sleep( 200 );
            }

        } catch( Exception ex )
        {
            ex.printStackTrace();
        }

        wr.lastRow();

        System.out.println("Sended to the server: " + wr.getProcessed() + "/" + wr.getTotal() );


        // Create a notification
        Notification.create(
                "Processed sheet " + cSheet + "/" + sheetAmount + ". <b>Phaser.setDataSheet</b>. Total rows: " + gridRows.size(),
                fileKey,
                2
        );
    }

    /**
     * Check city
     *
     * @param city
     * @return
     */
    private String cityCheck( Integer city )
    {
        // if the city is equal to zero
        if( city == 0 )
        {
            // get the default city
            return opt.getOptionS( "city_default" );
        }

        return city.toString();
    }

    /**
     * Find the name of the city in the value
     *
     * @return int. Id of the city and priority
     */
    private Integer[] handlerCity( String value, Boolean in_names, Integer[] city_region, String key )
    {
        if( ! in_names )
        {
            // get priority for the region
            int priority_city = cCity.getPriority( key, city_region[3] );

            if( priority_city > city_region[3] )
            {
                // set new priority
                city_region[3] = priority_city;

                // get region
                city_region = cCity.getRegion( value, city_region );
            }
        }

        // get city and return
        return cCity.getCity( value, in_names, city_region );
    }

    /**
     * Handler for processing the type "month"
     *
     * @param s String
     * @return String result
     */
    private String handlerMonth( String s )
    {
        return s;
    }

    /**
     * Handler for processing the type "hyperlink"
     *
     * @param s String
     * @return String result
     */
    private String handlerHyperlink( String s )
    {
        // if this image
        if( this.isImage( s ) )
        {
            // downloading image by URL
//            this.downFile( s );
        }

        return s;
    }

    /**
     * Image definition by format
     *
     * @param s String
     * @return Boolean
     */
    private boolean isImage( String s )
    {
        // image formats
        String[] formats = {"jpg", "jpeg", "png", "gif", "bmp"};

        // getting the format from the URL
        s = this.getFileFormat( s );

        for( String f : formats )
            if( f.equals( s ) )
                return true;

        return false;
    }

    /**
     * Format definition by URL
     *
     * @param s String
     * @return String
     */
    private String getFileFormat( String s )
    {
        return Helper.getFileFormat( s );
    }

    /**
     * Downloading a file from a URL
     *
     * @param from String
     * @return Boolean
     */
    private boolean downFile( String from )
    {
        return Helper.downFile( from, "/" );
    }

}
