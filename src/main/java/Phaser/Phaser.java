package Phaser;

import Phaser.ClassObject.*;
import Phaser.Handlers.Type;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.xssf.usermodel.*;

import java.io.*;
import java.util.*;

public class Phaser{

    private XSSFWorkbook wb;

    // путь к активному файлу
    private String pathFile;

    // ключ и имя файла
    public static String fileKey;
    private static String fileName;

    // sheet name
    private static String sheetName;

    // всего листов
    private Integer sheetAmount = 0;

    // текущий лист
    private Integer cSheet = 0;

    // текущая строка
    static Map<Integer, String> cRow = new HashMap<>();

    // указатель на пустоту строки
    static Boolean rowNotEmpty = false;

    // Указатель на скрытую колонку
    private Boolean isColumnHidden = false;

    // массив для данных
    private List<GridRow[]> dataGridRows = new ArrayList<>();

    // массив для заголовков
    public static Map<Integer, ObjHeader> header = new HashMap<>();

    private Employment employment;

    // Handler the type
    private Type type_;

    /*
     * list of cities
     *
     * [i][0] - id
     * [i][1] - name
     * [i][2] - priority
     * [i][3] - region
     */
    private static String[][] cities;

    // city name from "city row"
    private static String cityName = "";

    //
    private static Option opt = new Option();

    /**
     * Запуск парсера
     *
     * @param grid - название файла
     */
    void run( String[] grid )
    {
        // get options from server
        opt.getAllOptionFromServer();

        // file key
        fileKey = grid[1];

        // file name
        fileName = grid[2];

        // path to file
        pathFile = Helper.getDir() + Option.DIR_SEP + grid[0];

        // get file format
        String exp = Helper.getFileFormat( grid[0] );

        // check if there are "hand_headers"
        int hand_headers = Integer.parseInt( grid[3] );
        if( hand_headers > 0 )
        {
            new Header().setHeaderManually( fileKey );
        }

        // check if there are "hand_employment"
        int hand_employment = Integer.parseInt( grid[4] );

        // object for employment
        employment = new Employment( fileKey, ( hand_employment > 0 ) );

        // handler type
        type_ = new Type( fileKey );

        // Определяю формат файла
        if( exp.equals( "xlsx" ) )
        {
            // get cities
            cities = RequestsToServer.getCities();

            // start reading file
            this.readXLSXFile();

            // Если не удалось определить разширение
        } else
        {
            // Уведомление из парсера
            Notification.create(
                    "Could not determine file extension. <b>Phaser.run</b>. This extension:" + exp,
                    fileKey,
                    0
            );
        }
    }

    /**
     * считывает xlsx файл
     */
    private void readXLSXFile()
    {
        try
        {
            // создаю поток
            InputStream ExcelFileToRead = new FileInputStream( pathFile );
            // читаю файл
            wb = new XSSFWorkbook( ExcelFileToRead );

            // для работы со строками
            XSSFRow row;
            // для работы с ячейками
            XSSFCell cell;

            // количество листов
            sheetAmount = wb.getNumberOfSheets();

            // итераторы для строки и ячейки
            Integer itrCell;

            // перебираю листы
            for( int i = 0; i < sheetAmount; i++ )
            {
                // текущий лист
                cSheet = i;

                // get sheet
                XSSFSheet sheet = wb.getSheetAt( i );

                // get sheet name
                sheetName = sheet.getSheetName();

                // объединение объединенных ячеек
                MargeCell.unMerge( sheet );

                // получаю строки
                Iterator rows = sheet.rowIterator();

                // перебираю строки
                while( rows.hasNext() )
                {
                    // активная строка
                    row = ( XSSFRow ) rows.next();

                    // указываю что строка пустая
                    rowNotEmpty = false;

                    // получаю ячейки
                    Iterator cells = row.cellIterator();

                    // если нету ячеек, пропускаю строку
                    if( row.getLastCellNum() < 1 )
                        continue;

                    // перебираю ячейки
                    while( cells.hasNext() )
                    {
                        // активная ячейка
                        cell = ( XSSFCell ) cells.next();

                        // итератор ячейки
                        itrCell = cell.getColumnIndex();

                        // указыват на скрытую колонку
                        isColumnHidden = sheet.isColumnHidden( itrCell );

                        // получаю и записываю значение ячейки
                        cRow.put( itrCell, this.detectTypeAndGetValue( cell ) );
                    }

                    /*
                      Проверяю наличие заголовка,
                       и пустоту строки.
                     */
                    if( Header.hasHeader() && rowNotEmpty )
                    {
                        // записываю данные
                        this.setRow();
                    } else if(! Header.hasHeader() )
                    {
                        // создаю херед
                        new Header().createHeader();
                    }

                    cRow.clear();
                }

                /*
                  В конце листа,
                  записываю обработанные данные на сервер.
                 */
                setDataSheet();
            }
        } catch( IOException ex )
        {
            // Уведомление из парсера
            Notification.create(
                    "Exception when reading the grid. <b>Phaser.readXLSXFile</b>. Message:" + Helper.getStackTrace( ex ),
                    fileKey,
                    0
            );
        }
    }

    /**
     * Определение типя ячейки и получение значения
     *
     * @param cell - ячейка
     */
    private String detectTypeAndGetValue( XSSFCell cell )
    {
        String value;

        // итератор ячейки
        int itrCell = cell.getColumnIndex();

        // if the cell is a formula
        if (cell.getCellType() == Cell.CELL_TYPE_FORMULA) {
            switch (cell.getCachedFormulaResultType()) {
                case Cell.CELL_TYPE_NUMERIC: return String.valueOf( cell.getNumericCellValue() );
                case Cell.CELL_TYPE_STRING: return cell.getStringCellValue().replaceAll("'", "");
            }
        }

        // определяю тип ячейки
        switch( cell.getCellType() )
        {

            case XSSFCell.CELL_TYPE_STRING:
                // получаю значение ячейки
                value = handleValueXSSF(
                        cell.getStringCellValue(),
                        cell,
                        itrCell
                );
                rowNotEmpty = true;
                break;

            case XSSFCell.CELL_TYPE_NUMERIC:
                // если это дата
                if( DateUtil.isCellDateFormatted( cell ) )
                {
                    // получаю дату с ячейки
                    Date date = cell.getDateCellValue();

                    // получаю месяц
                    int month = date.getMonth() + 1;
                    // получаю год
                    int year = date.getYear() + 1900;

                    value = month + "." + year;
                } else
                {
                    // получаю значение ячейки
                    value = this.handleValueXSSF(
                            String.valueOf( cell.getNumericCellValue() ),
                            cell,
                            itrCell
                    );
                }
                rowNotEmpty = true;
                break;

            case XSSFCell.CELL_TYPE_BOOLEAN:
                // получаю значение ячейки
                value = this.handleValueXSSF(
                        String.valueOf( cell.getBooleanCellValue() ),
                        cell,
                        itrCell
                );
                rowNotEmpty = true;
                break;

            case XSSFCell.CELL_TYPE_ERROR:
            case XSSFCell.CELL_TYPE_BLANK:
            default:
                // получаю значение ячейки
                value = this.handleValueXSSF(
                        "",
                        cell,
                        itrCell
                );
                break;
        }

        return value;
    }

    /**
     * обработчик значений ячейки
     * @param value
     * @param cell
     * @param itr
     * @return
     */
    private String handleValueXSSF( String value, XSSFCell cell, Integer itr )
    {
        // if the column is hidden
        if( isColumnHidden ) return "hide";

        // if the cell is not defined
        if( cell == null ) return "";

        // get the type of cell by id
        String type = Header.getTypeCell( itr );

        // if the type is not empty
        if( type != null )
        {
            switch( type )
            {
                case "month":
                    // вытягиваю цвет фона
                    XSSFColor color = cell.getCellStyle().getFillForegroundColorColor();

                    if( color != null )
                    {
                        // пытаюсь перевести цвет в определение занятости
                        value = employment.colorDetermination( value, color.getARGBHex() );
                    } else
                    {
                        // пытаюсь перевести цвет в определение занятости
                        value = employment.colorDetermination( value, "ffffff" );
                    }
                    break;

                case "hyperlink":
                    // вытягиваю гиперссылку
                    XSSFHyperlink link = cell.getHyperlink();

                    // проверяю наличие ссылки
                    if( link != null )
                    {
                        // получаю адресс
                        value = link.getAddress();

                        // если ссылки не оказалось, проверяю на пустоту
                    } else if( value.length() > 0 )
                    {
                        // пытаюсь получить ссылку из строки
                        value = linkBetween( value );
                    }
                    break;

                case "type":

                    // Type determination
                    value = type_.find( value );

                    break;

                case "light":

                    //
                    boolean defined = false;

                    // to lower case
                    value = value.toLowerCase().trim();

                    // get an option, a positive state of light
                    String[] light_exist = opt.getOption( "light_exist" );

                    for( String light : light_exist )
                    {
                        // if the value is equal to light
                        if( light.toLowerCase().trim().equals( value ) )
                        {
                            defined = true;
                            value = "true";
                        }
                    }

                    value = defined ? value : "false";

                    break;
            }
        }

        return value.trim();
    }

    /**
     * Запись строки в список, с название заголовка
     */
    private void setRow()
    {
        // create a new array for the row
        GridRow[] row_tmp = new GridRow[cRow.size() + 2];

        // minimum number of empty cells
        int added_to_row = Option.ROW_MINIMUM_NULL_VALUE;

        // iterator for the tmp
        int k = 0;

        for( Map.Entry<Integer, String> row : cRow.entrySet() )
        {
            // cell iterator
            Integer i = row.getKey();

            // if there is no null
            if( header.get( i ) != null )
            {
                // check if the value is not a header
                if( isNotHeader( header.get( i ).name, row.getValue() ) )
                {
                    // if the value is not null
                    if( row.getValue().length() > 0 ) added_to_row++;

                    // add to temporary array
                    row_tmp[k++] = new GridRow( header.get( i ).type, row.getValue(), header.get( i ).key );
                }
            }
        }

        /*
          Стравнение елементов строки с елементами заголовков.
          Елементов строки должно быть столько же, сколько было заявлено заголовков
         */
        if( header.size() <= added_to_row )
        {
            // add city identifier
            row_tmp[cRow.size() + 1] = new GridRow( "city", cityName, "city_name" );

            // add to list
            dataGridRows.add( row_tmp );
        }
        // if values are added
        else if( added_to_row > 0 )
        {
            isCityRow();
        }
    }

    /**
     * Поиск города в строке,
     * которая не подошла по шаблону для входных данных
     */
    static void isCityRow()
    {
        int found = 0;

        for( Map.Entry<Integer, String> v : cRow.entrySet() )
        {
            String value = v.getValue().trim().toLowerCase();

            // word filter
            if( skipThisCity( value ) ) continue;

            // if this value is not null
            if( value.length() > 0 )
            {
                for( String[] _city : cities )
                {
                    // divided the city into ru and ua
                    String[] _cities = _city[1].split( "\\|\\|" );

                    for( String city : _cities )
                    {
                        // find the name of the city in the value
                        if( value.contains( city.toLowerCase() )  )
                        {
                            found++;
                            // city name
                            cityName = city;
                        }
                    }
                }
            }
        }

        // if more than one city is found
        if( found > 1 ){
            cityName = "";
        }
    }

    /**
     * The filter of word.
     * @param value
     * @return boolean
     */
    private static boolean skipThisCity( String value )
    {
        // word list for skipping
        String[] word_skip = opt.getOption( "word_skip" );

        for( String word : word_skip )
            if( value.contains( word.trim() ) )
                return true;

        return false;
    }

    /**
     * Проверяет заголовок и значение ячейки на совпадение.
     * Для того, чтобы заголовки не воспринимались как входны данные.
     *
     * @param headName - header name
     * @param value    - value of the cell
     */
    private Boolean isNotHeader( String headName, String value )
    {
        // Checking the name of the header and value
        return ! headName.equals( value.trim().toLowerCase() );
    }

    /**
     * Cutting the link from a string
     */
    private String linkBetween( String value )
    {
        // try cutting the link
        String _value = StringUtils.substringBetween( value, "HYPERLINK(\"", "\",\"" );

        // если не удолось вырезать ссылку, оставлю строку пустой
        return _value == null ? value : _value;
    }

    /**
     * Отправляю обработанный лист в Handler, в новом потоке
     */
    private void setDataSheet()
    {
//        for( GridRow[] gridRow : dataGridRows )
//            for( GridRow cell : gridRow )
//                if( cell != null )
//                    cell.print();


        System.out.println("Обработанно");

        // data handler in another thread
        new Handler(
                dataGridRows, // all the correct rows
                fileKey,      // file key
                sheetAmount,  // number of sheets
                cSheet,       // current sheet
                fileName,     // file name
                sheetName,    // sheet name
                header        // list of headers
        ).run();


        // cleaning arrays
        dataGridRows.clear();
        header.clear();
    }

}