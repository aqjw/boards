import Phaser.RequestsToServer;

import javax.swing.*;
import java.awt.event.*;

public class Main extends JDialog{
    private JPanel contentPane;
    private JButton btnExit;

    public Main(){
        setContentPane( contentPane );
        setModal( true );

        btnExit.addActionListener( e -> System.exit( 0 ) );
    }


    public static void main( String[] args ){

//        System.exit( 0 );

        RequestsToServer.gridReader();

        Main dialog = new Main();
        dialog.pack();
        dialog.setVisible( true );
    }
}
